package com.hw.db.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Message;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

public class ThreadControllerTests {

    @MockBean
    private final threadController threadController = new threadController();


    private static final MockedStatic<ThreadDAO> THREAD_DAO = mockStatic(ThreadDAO.class);
    private static final MockedStatic<UserDAO> USER_DAO = mockStatic(UserDAO.class);

    @BeforeEach
    void setUp() {
        when(ThreadDAO.getThreadById(Mockito.any())).thenReturn(new Thread());
        when(ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(new Thread());
        User user = new User();
        user.setNickname("n");
        when(UserDAO.Info(Mockito.anyString())).thenReturn(user);
    }

    @Test
    void checkIdOrSlugTest() {
        String parameter = "slug_example";

        threadController.CheckIdOrSlug(parameter);
        THREAD_DAO.verify(
            Mockito.times(1),
            () -> ThreadDAO.getThreadBySlug(parameter)
        );
    }

    @Test
    void checkIdOrSlugIntTest() {
        String parameter = "1";

        threadController.CheckIdOrSlug(parameter);
        THREAD_DAO.verify(
            Mockito.times(3),
            () -> ThreadDAO.getThreadById(Integer.parseInt(parameter))
        );
        THREAD_DAO.verifyNoMoreInteractions();
    }

    @Test
    void createPostTest() {
        String slug = "slug";
        List<Post> posts = Collections.emptyList();

        threadController.createPost(slug, posts);

        THREAD_DAO.verify(
            Mockito.atLeastOnce(),
            () -> ThreadDAO.createPosts(Mockito.any(), eq(posts), Mockito.anyList())
        );
    }

    @Test
    void getPostsTest() {
        String slug = "1";
        Integer limit = 1;
        Integer since = 1;
        String sort = "id";
        Boolean desc = false;

        threadController.Posts(slug, limit, since, sort, desc);

        THREAD_DAO.verify(
            Mockito.times(1),
            () -> ThreadDAO.getPosts(null, limit, since, sort, desc)
        );
    }

    @Test
    void changeTest() {
        String slug = "1";
        Thread thread = new Thread();
        thread.setId(1);

//        when(threadController.CheckIdOrSlug(Mockito.anyString())).thenReturn(thread);
        when(ThreadDAO.getThreadById(Mockito.any())).thenReturn(thread);
        when(ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(thread);


        threadController.change(slug, thread);

        THREAD_DAO.verify(
            Mockito.times(1),
            () -> ThreadDAO.change(thread, thread)
        );
    }

    @Test
    void infoTest() {
        String slug = "1";

        threadController.info(slug);

        THREAD_DAO.verify(
            Mockito.times(4),
            () -> ThreadDAO.getThreadById(Integer.parseInt(slug))
        );
    }


    @Test
    void createVote() {
        String slug = "slug";

        THREAD_DAO
            .when(() -> ThreadDAO.getThreadBySlug(slug))
            .thenThrow(EmptyResultDataAccessException.class);

        assertEquals(HttpStatus.NOT_FOUND, threadController.createVote(slug, mock(Vote.class)).getStatusCode());
    }
}
